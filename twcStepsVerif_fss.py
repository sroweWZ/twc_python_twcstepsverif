#!/usr/bin/python3

# --------------------------------------------------------------------------------------------------------------------------------------
#
# TWC Steps Verification
#
# This script:
#	- loads a list of radar IDs
#	- watchs for their ppis or cappis to take as a "truth"
#	- locates all the twc steps forecasts that correspond to a truth
#	- calculate stats comparing the truth to the forecast(s)
#	- update running stats for the radar
#
# --------------------------------------------------------------------------------------------------------------------------------------

from datetime import datetime
from glob import glob
import h5py
import logging
import netCDF4 as nc
import os
from subprocess import call
import sys
#import threading
import time
from watchdog.observers.polling import PollingObserver
from watchdog.events import FileSystemEventHandler
import yaml

from libFSS import statsFSS

#----------------------------------------------------------------------------------------#

def syscall(cmd):
	return call(cmd, shell=True, stderr=open(os.devnull, 'w'), stdout=open(os.devnull, 'w'), close_fds=True)

# threaded watch execution
class MyWatch(FileSystemEventHandler):

	def __init__(self, id_):
		self.id = id_
		self.pattern = 'IDR{}'.format(id_)


	def on_modified(self, event):

		# file may just have been deleted, in which case we care not for its modification
		if os.path.exists(event.src_path):
			self.on_created(event)


	def on_created(self, event):

		obsFile = event.src_path

		# does the created file, not directory, match our watch pattern?
		if not event.is_directory and self.pattern in obsFile:

			# directories, filenames
			obsDir = '{}/{}'.format(cfg['twcStepsVerif']['obsPath'], self.id)
			fcDir = '{}/{}'.format(cfg['twcStepsVerif']['fcPath'], self.id)
			statsDir = '{}/{}'.format(cfg['twcStepsVerif']['statsPath'], self.id)

			logging.info('runTwcStepsVerif verifying forecasts against {}'.format(obsFile))

			# create stats directory if it doesn't exist
			if not os.path.exists(statsDir):
				os.makedirs(statsDir)

			# ingest the obs time and date into numpy
			ncf = nc.Dataset(obsFile, 'r', format='NETCDF4_CLASSIC')
			(obsTime, obsLons, obsLats, obsDbzh) = (ncf.variables['time'][:],
													ncf.variables['lon'][:],
													ncf.variables['lat'][:],
													ncf.variables['refl'][:])
			ncf.close()

			# glob the twcSteps forecast files that have the same forecast date
			# here we hope/assume that the forecast dates twcSteps outputs correspond to radar dates (from time to time at least)
			# ll data/70/twcSteps_*_${d}.nc

			# calculate stats for each corresponding forecast
			obsTimeStr = datetime.fromtimestamp(obsTime[0]-30).strftime('%Y%m%d%H%M00')
			logging.debug('Looking for {}/*_{}.nc'.format(fcDir, obsTimeStr))
			for fcFile in sorted(glob('{}/*_{}.nc'.format(fcDir, obsTimeStr))):

				# ingest the forecast into numpy
				ncf = nc.Dataset(fcFile, 'r', format='NETCDF4_CLASSIC')
				(fcTime, fcLons, fcLats, fcDbzh) = (ncf.variables['time'][:],
													ncf.variables['lon'][:],
													ncf.variables['lat'][:],
													ncf.variables['refl'][:])
				ncf.close()
			
				# possibly some inteprolation to a common grid would be performed here
				# but for now we assume that they are the same

				# crop grid to around the radar to improve the fairness of the comparison
				# TODO

				# send off the data for statistics calculation, writing results to file

				# fractional skill score
				logging.debug('Calculating fractional skill score between {} and {}'.format(obsFile, fcFile))
				statsFile = '{}/stats_{}.fss.yaml'.format(statsDir, obsTimeStr)
				windows = [32,64,96,128,160,192,224,256]	# in pixels (1024p/256km => 4p/km)
				levels = [10,20,30,40]		# in dBZ
				num_data, den_data, fss_data = statsFSS(fcDbzh[0,:,:], obsDbzh[0,:,:,0], windows, levels)
				print(fss_data)
#				with open(statsFile, 'w') as outfile:
#					yaml.dump([num_data, den_data, fss_data], outfile, default_flow_style=False)


				# TODO dump a diagnostic netCDF with comparison fields TODO


			# update any running statistics TODO
#			yaml.load(ymlfile)
#			stream = file('document.yaml', 'w')
#			yaml.dump(data, stream)

			logging.info('Updating running statistics'.format())


			logging.info('twcStepsVerif complete for {}'.format(obsFile))

#			# age oldest twcStepsVerif output files
#			syscall('find {} -type f -maxdepth 1 -mmin +{} -exec rm {{}} \;'.format(outputDir, cfg['twcStepsVerif']['ageingDelay']))


#----------------------------------------------------------------------------------------#

# main watch loop
if __name__ == "__main__":

	# load config file
	with open(sys.argv[1], 'r') as ymlfile:
		cfg = yaml.load(ymlfile)

	# setup logging
	logging.basicConfig(filename=cfg['twcStepsVerif']['logFile'], level=logging.getLevelName(cfg['twcStepsVerif']['logLevel']), format='%(asctime)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
	logging.debug('Config file {} read on startup'.format(sys.argv[1]))
	logging.debug('Polling set at {} seconds'.format(cfg['twcStepsVerif']['pollingInterval']))

	# setup observer. must be polling to work on NFS
	observer = PollingObserver(timeout=cfg['twcStepsVerif']['pollingInterval'])
	observer.start()

	# dictionnary of watchs, empty to begin with
	watch = dict();

	# sit and watch
	try:
		while True:

			# add or remove any watches if changes to the ID list are detected
			if set(watch.keys()) != set(cfg['twcStepsVerif']['ids']):

				# ids to add, but only if the watch directory exists already
				for id_ in list(set(cfg['twcStepsVerif']['ids']) - set(watch.keys())):
					watchPath = '{}/{}'.format(cfg['twcStepsVerif']['obsPath'], id_)
					if os.path.exists(watchPath):
						watch[id_] = observer.schedule(MyWatch(id_), path=watchPath, recursive=False)
						logging.info('IDR{} added to twcStepsVerif watch'.format(id_))

				# ids to remove
				for id_ in list(set(watch.keys()) - set(cfg['twcStepsVerif']['ids'])):
					observer.unschedule(watch[id_])					
					watch.pop(id_, None)
					logging.info('IDR{} removed from twcStepsVerif Watch'.format(id_))

			# wait for the next config file update check
			# note that this is a non-blocking sleep, files are still watched
			time.sleep(cfg['twcStepsVerif']['configRefresh'])

			# load config file
			with open(sys.argv[1], 'r') as ymlfile:
				cfg = yaml.load(ymlfile)
				logging.getLogger().setLevel(cfg['twcStepsVerif']['logLevel'])
				logging.debug('Config file {} updated'.format(sys.argv[1]))
				logging.debug('Polling set at {} seconds'.format(cfg['twcStepsVerif']['pollingInterval']))

	except KeyboardInterrupt:
		observer.stop()

	# clean up 
	observer.join()

