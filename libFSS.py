"""
.. module:: libFSS
:platform: Unix
:synopsis: Compute the fraction skill score (2D).
.. moduleauthor:: Nathan Faggian <n.faggian@bom.gov.au>
"""

# Fast fractional skill score calulation
# http://metnet.imd.gov.in/mausamdocs/166310_F.pdf

import numpy as np

def compute_integral_table(field):
	return field.cumsum(1).cumsum(0)


def integral_filter(field, n, table=None):
	"""
	Fast summed area table version of the sliding accumulator.
	:param field: nd-array of binary hits/misses.
	:param n: window size.
	"""

	w = n // 2
	if w < 1.:
		return field

	if table is None:
		table = compute_integral_table(field)

	r, c = np.mgrid[0:field.shape[0], 0:field.shape[1]]
	r = r.astype(np.int)
	c = c.astype(np.int)
	w = np.int(w)
	r0, c0 = (np.clip(r - w, 0, field.shape[0] - 1), np.clip(c - w, 0, field.shape[1] - 1))
	r1, c1 = (np.clip(r + w, 0, field.shape[0] - 1), np.clip(c + w, 0, field.shape[1] - 1))
	integral_table = np.zeros(field.shape).astype(np.int64)
	integral_table += np.take(table, np.ravel_multi_index((r1, c1), field.shape))
	integral_table += np.take(table, np.ravel_multi_index((r0, c0), field.shape))
	integral_table -= np.take(table, np.ravel_multi_index((r0, c1), field.shape))
	integral_table -= np.take(table, np.ravel_multi_index((r1, c0), field.shape))

	return integral_table


def fss(fcst, obs, threshold, window, fcst_cache=None, obs_cache=None):
	"""
	Compute the fraction skill score using summed area tables .
	:param fcst: nd-array, forecast field.
	:param obs: nd-array, observation field.
	:param window: integer, window size.
	:return: tuple of FSS numerator, denominator and score.
	"""
	fhat = integral_filter(fcst > threshold, window, fcst_cache)
	ohat = integral_filter(obs > threshold, window, obs_cache)
	num = np.nanmean(np.power(fhat - ohat, 2))
	denom = np.nanmean(np.power(fhat, 2) + np.power(ohat, 2))

	return num, denom, 1.-num/denom


def statsFSS(fcst, obs, windows, levels):
	"""
	Compute the fraction skill scores over different levels and window sizes
	:param fcst: nd-array, forecast field.
	:param obs: nd-array, observation field.
	:param window: list, window sizes.
	:param levels: list, threshold levels.
	:return: results the FSS: numerator,denominator and score.
	"""
	num_data, den_data, fss_data = [], [], []

	for level in levels:
		ftable = compute_integral_table(fcst > level)
		otable = compute_integral_table(obs > level)

		_data = [fss(fcst, obs, level, w, ftable, otable) for w in windows]

		num_data.append([x[0] for x in _data])
		den_data.append([x[1] for x in _data])
		fss_data.append([x[2] for x in _data])

	return num_data, den_data, fss_data

