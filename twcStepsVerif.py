#!/usr/bin/python3

# --------------------------------------------------------------------------------------------------------------------------------------
#
# TWC Steps Verification
#
# This script:
#	- loads a list of radar IDs
#	- watchs for their ppis or cappis
#	- calculates time until onset/termination of rain based on this and preceeding ppis/cappis
#	- compares this to the forecast values
#	- update running stats for the radar
#
# --------------------------------------------------------------------------------------------------------------------------------------

from datetime import datetime
from glob import glob
import h5py
import logging
import multiprocessing
import netCDF4 as nc
import numpy as np
import os
import re
from subprocess import call
import sys
#import threading
import time
import warnings
from watchdog.observers.polling import PollingObserver
from watchdog.events import FileSystemEventHandler
import yaml

import libTwcStepsVerif
#from libFSS import statsFSS

#----------------------------------------------------------------------------------------#

def syscall(cmd):
	return call(cmd, shell=True, stderr=open(os.devnull, 'w'), stdout=open(os.devnull, 'w'), close_fds=True)

# threaded watch execution
class MyWatch(FileSystemEventHandler):

	def __init__(self, id_):
		self.id = id_

		# choose pattern as a function of verification mode (operational or hindcast)
		if cfg['twcStepsVerif']['operationalMode']:
			self.pattern = 'twcSteps_reanalysis_{}_'.format(id_)
		else:
			self.pattern = 'twcSteps_fc_{}_'.format(id_)

	def on_modified(self, event):

		# file may just have been deleted, in which case we care not for its modification
		if os.path.exists(event.src_path):
			self.on_created(event)

	def on_created(self, event):

		# does the created file, not directory, match our watch pattern?
		if not event.is_directory and self.pattern in event.src_path:

			# divi out work to separate processes. note calling active_children() will conveniently clean up any zombie processes
			while len(multiprocessing.active_children()) >= cfg['twcStepsVerif']['threadLimit']:
				time.sleep(cfg['twcStepsVerif']['threadPollingInterval'])

			logging.debug('Thread {} of maximum {}'.format(len(multiprocessing.active_children()) + 1, cfg['twcStepsVerif']['threadLimit']))
			multiprocessing.Process(name='twcStepsVerifThread', target=self.runTwcStepsVerif, args=(event,)).start()


	def runTwcStepsVerif(self, event):

		watchFile = event.src_path

		# does the created file, not directory, match our watch pattern?
		if not event.is_directory and self.pattern in watchFile:

			# directories, filenames, lengths
			reaDir = '{}/{}'.format(cfg['twcStepsVerif']['reanalysisPath'], self.id)
			fcDir = '{}/{}'.format(cfg['twcStepsVerif']['fcPath'], self.id)
			statsDir = '{}/{}'.format(cfg['twcStepsVerif']['statsPath'], self.id)
			ndbz, nt = len(cfg['twcStepsVerif']['outputDBzThresholds']), cfg['twcStepsVerif']['nLeadtimes']

			# create stats directory if it doesn't exist
			if not os.path.exists(statsDir):
				os.makedirs(statsDir)

			# glean analysis time string from the watched file
			analysisTimeStr = re.search('twcSteps_.*_.*_([0-9]{14})\.nc', watchFile).group(1)				# TODO don't assume a match ^^

			# determine the corresponding forecast/reanalysis files or vice versa depending on the verification mode (operational or hindcast)
			if cfg['twcStepsVerif']['operationalMode']:
				fcFile = '{}/twcSteps_fc_{}_{}.nc'.format(fcDir, self.id, analysisTimeStr)
				reaFile = watchFile
			else:
				reaFile = '{}/twcSteps_reanalysis_{}_{}.nc'.format(reaDir, self.id, analysisTimeStr)
				fcFile = watchFile

			# setup statistics output file
			statsFile = '{}/stats_{}.csv'.format(statsDir, analysisTimeStr)
			csvTxt = '# Stats for forecast analysis {}\n'.format(analysisTimeStr)
			csvTxt += ('# ForDate, ObsTime, Leadtime, dBZThreshold, nObsRain,'
						' OnsetME, OnsetMAE, OnsetPositivePredictiveValue, OnsetNegativePredictiveValue, OnsetFalseDiscoveryRate, OnsetFalseOmissionRate,'
						' RainTermME, RainTermMAE, TermPositivePredictiveValue, TermNegativePredictiveValue, TermFalseDiscoveryRate, TermFalseOmissionRate\n')

			# ingest the fc netCDF for comparison with reanalysis
			fcTimes, fcLons, fcLats, fcTimeUntilRainOnset = libTwcStepsVerif.readNetCdfVar(fcFile, ['timeUntilRainOnset'])
			nx, ny = len(fcLons), len(fcLats)
			fcTimeUntilRainOnset = np.nan * np.ones((nt, ndbz, nx, ny))
			fcTimeUntilRainTermination = np.nan * np.ones_like(fcTimeUntilRainOnset)
			fcTimes, fcLons, fcLats, data = libTwcStepsVerif.readNetCdfVar(fcFile, ['timeUntilRainOnset'])
			fcTimeUntilRainOnset = data['timeUntilRainOnset'][:]
			fcTimes, fcLons, fcLats, data = libTwcStepsVerif.readNetCdfVar(fcFile, ['timeUntilRainTermination'])
			fcTimeUntilRainTermination = data['timeUntilRainTermination'][:]
#			_, _, _, data = libTwcStepsVerif.readNetCdfVar(fcFile, ['distanceToRain'])
#			fcDistanceToRain = data['distanceToRain'][:]
#			_, _, _, data = libTwcStepsVerif.readNetCdfVar(fcFile, ['ddist_dx'])
#			fcDDistDx = data['ddist_dx'][:]
#			_, _, _, data = libTwcStepsVerif.readNetCdfVar(fcFile, ['ddist_dy'])
#			fcDDistDy = data['ddist_dy'][:]
#			_, _, _, data = libTwcStepsVerif.readNetCdfVar(fcFile, ['u'])
#			fcu = data['u'][:]
#			_, _, _, data = libTwcStepsVerif.readNetCdfVar(fcFile, ['v'])
#			fcv = data['v'][:]

			# ingest the reanalysis netCDF for comparison with the forecast
			_, reaLons, reaLats, _ = libTwcStepsVerif.readNetCdfVar(reaFile, ['timeUntilRainOnset'])
			nx, ny = len(reaLons), len(reaLats)
			reaTimeUntilRainOnset = np.nan * np.ones((nt, ndbz, nx, ny))	# assume the same dimensions as the forecast
			reaTimeUntilRainTermination = np.nan * np.ones_like(reaTimeUntilRainOnset)

			reaTimes, reaLons, reaLats, data = libTwcStepsVerif.readNetCdfVar(reaFile, ['timeUntilRainOnset'])
			reaTimeUntilRainOnset = data['timeUntilRainOnset'][:]
			reaTimes, reaLons, reaLats, data = libTwcStepsVerif.readNetCdfVar(reaFile, ['timeUntilRainTermination'])
			reaTimeUntilRainTermination = data['timeUntilRainTermination'][:]

			# dt is expected to be either 6 or 10 minutes  (in seconds)
			# TODO this is perhaps a tad sloppy
			dt = np.round(np.mean(np.diff(fcTimes)) / 60.0) * 60	# requires several fcTimes to be reflective....
			
			# TODO check that times "roughly" correspond between reanalysis and forecast

			# TODO possibly some interpolation to a common grid would be performed here although this is currently not necessary as forecasts are on on the same grid as the input radar data

			# optionally crop grid to around the radar centre to improve the fairness of the comparison
			mask = np.ones((nx,ny))
#			mask = np.nan * np.ones((nx,ny))
#			mask[(1*nx/4):(3*nx/4),(1*ny/4):(3*ny/4)] = 1.0
			mask[(3*nx/8):(5*nx/8),(3*ny/8):(5*ny/8)] = np.nan
#			(mlons, mlats) = np.meshgrid(fcLons, fcLats)
#			mask[np.logical_and(abs(mlons - 151.297) <= 0.2, abs(mlats - -32.833) <= 0.2)] = 1.0	# Cessnock
#			mask[np.logical_and(abs(mlons - 149.817) <= 0.2, abs(mlats - -34.383) <= 0.2)] = 1.0	# Taralga

			# compare "time until onset/termination of rain" fields
			# only compare areas with observed and forecast, non-nan values
			for dBZi in	range(ndbz):

				# subset to desired dBZ and first timestep whilst applying geographical mask
				fcTimeUntilRainOnset_ = fcTimeUntilRainOnset[0,dBZi,:,:] * mask
				reaTimeUntilRainOnset_ = reaTimeUntilRainOnset[0,dBZi,:,:] * mask
				fcTimeUntilRainTermination_ = fcTimeUntilRainTermination[0,dBZi,:,:] * mask
				reaTimeUntilRainTermination_ = reaTimeUntilRainTermination[0,dBZi,:,:] * mask

				for t in range(nt):

					leadtime = t*dt
					fcTimeStr = datetime.utcfromtimestamp(fcTimes[t]).strftime('%Y%m%d%H%M%S')
					reaTimeStr = datetime.utcfromtimestamp(reaTimes[t]).strftime('%Y%m%d%H%M%S')

#					fcDistanceToRain_ = fcDistanceToRain[t,:,:]
#					fcDDistDx_ = fcDDistDx[t,:,:]
#					fcDDistDy_ = fcDDistDy[t,:,:]

					# pixels with delay equal to desired leadtime
					fcOnsetPixels = fcTimeUntilRainOnset_ == leadtime
#					reaOnsetPixels = reaTimeUntilRainOnset_ == leadtime		# modified see below
					reaOnsetPixels = ~np.isnan(reaTimeUntilRainOnset_)		# a more relaxed definition for more general predictive analysis stats
					fcTermPixels = fcTimeUntilRainTermination_ == leadtime
#					reaTermPixels = reaTimeUntilRainTermination_ == leadtime	# modified see below
					reaTermPixels = ~np.isnan(reaTimeUntilRainTermination_)		# a more relaxed definition for more general predictive analysis stats

					reaPopulationOnset = np.sum(reaOnsetPixels)

					# empty arrays, /0 etc may generate warnings. ignore them
					with warnings.catch_warnings():
						warnings.simplefilter("ignore")

						# calculate onset and termination ME and MAE in seconds
						onsetError = reaTimeUntilRainOnset_[fcOnsetPixels] - fcTimeUntilRainOnset_[fcOnsetPixels]
						onsetME = np.mean(onsetError[~np.isnan(onsetError)])
						onsetMAE = np.mean(np.abs(onsetError[~np.isnan(onsetError)]))

						termError = reaTimeUntilRainTermination_[fcTermPixels] - fcTimeUntilRainTermination_[fcTermPixels]
						termME = np.mean(termError[~np.isnan(termError)])
						termMAE = np.mean(np.abs(termError[~np.isnan(termError)]))
				
						# calculate false positive and false negative rates, onset and termination
						onsetTruePositive = np.sum(np.logical_and(fcOnsetPixels, reaOnsetPixels))	# forecast and observed
						onsetFalsePositive = np.sum(np.logical_and(fcOnsetPixels, ~reaOnsetPixels))	# forecast but not observed
						onsetFalseNegative = np.sum(np.logical_and(~fcOnsetPixels, reaOnsetPixels))	# observed but not forecast
						onsetTrueNegative = np.sum(np.logical_and(~fcOnsetPixels, ~reaOnsetPixels))	# not observed and not forecast
						onsetPredictedConditionPositive = onsetTruePositive + onsetFalsePositive
						onsetPredictedConditionNegative = onsetTrueNegative + onsetFalseNegative

						onsetPositivePredictiveValue = onsetTruePositive / onsetPredictedConditionPositive
						onsetNegativePredictiveValue = onsetTrueNegative/ onsetPredictedConditionNegative
						onsetFalseDiscoveryRate = onsetFalsePositive / onsetPredictedConditionPositive
						onsetFalseOmissionRate = onsetFalseNegative/ onsetPredictedConditionNegative
		
						termTruePositive = np.sum(np.logical_and(fcTermPixels, reaTermPixels))		# forecast and observed
						termFalsePositive = np.sum(np.logical_and(fcTermPixels, ~reaTermPixels))	# forecast but not observed
						termFalseNegative = np.sum(np.logical_and(~fcTermPixels, reaTermPixels))	# observed but not forecast
						termTrueNegative = np.sum(np.logical_and(~fcTermPixels, ~reaTermPixels))	# not observed and not forecast
						termPredictedConditionPositive = termTruePositive + termFalsePositive
						termPredictedConditionNegative = termTrueNegative + termFalseNegative

						termPositivePredictiveValue = termTruePositive / termPredictedConditionPositive
						termNegativePredictiveValue = termTrueNegative/ termPredictedConditionNegative
						termFalseDiscoveryRate = termFalsePositive / termPredictedConditionPositive
						termFalseOmissionRate = termFalseNegative / termPredictedConditionNegative

#						meanWindDir = np.arctan2(np.mean(fcv[np.logical_and(~fcOnsetPixels, reaOnsetPixels)]), np.mean(fcu[np.logical_and(~fcOnsetPixels, reaOnsetPixels)]))
#						meanDistToRain = np.mean(fcDistanceToRain_[np.logical_and(~fcOnsetPixels, reaOnsetPixels)])
#						meanAngleToRain = np.arctan2(np.mean(fcDDistDy_[np.logical_and(~fcOnsetPixels, reaOnsetPixels)]), np.mean(fcDDistDx_[np.logical_and(~fcOnsetPixels, reaOnsetPixels)]))

						meanWindDir = 0.0 #np.arctan2(np.mean(fcv[np.logical_and(fcOnsetPixels, ~reaOnsetPixels)]), np.mean(fcu[np.logical_and(fcOnsetPixels, ~reaOnsetPixels)]))
						meanDistToRain = 0.0 #np.mean(fcDistanceToRain_[np.logical_and(fcOnsetPixels, ~reaOnsetPixels)])
						meanAngleToRain = 0.0 #np.arctan2(np.mean(fcDDistDy_[np.logical_and(fcOnsetPixels, ~reaOnsetPixels)]), np.mean(fcDDistDx_[np.logical_and(fcOnsetPixels, ~reaOnsetPixels)]))

					# send statistics to csv text
					csvTxt += '%s, %s, %d, %d, %d, %.0f, %.0f, %.2f, %.2f, %.2f, %.2f, %.0f, %.0f, %.2f, %.2f, %.2f, %.2f, %.2f, %.2f, %.2f\n'% (fcTimeStr, 
								reaTimeStr,
								leadtime,
								cfg['twcStepsVerif']['outputDBzThresholds'][dBZi],
								np.asscalar(reaPopulationOnset),
								np.asscalar(onsetME),
								np.asscalar(onsetMAE),
								np.asscalar(onsetPositivePredictiveValue),
								np.asscalar(onsetNegativePredictiveValue),
								np.asscalar(onsetFalseDiscoveryRate),
								np.asscalar(onsetFalseOmissionRate),
								np.asscalar(termME),
								np.asscalar(termMAE),
								np.asscalar(termPositivePredictiveValue),
								np.asscalar(termNegativePredictiveValue),
								np.asscalar(termFalseDiscoveryRate),
								np.asscalar(termFalseOmissionRate),
								meanDistToRain,
								meanAngleToRain,
								meanWindDir)

			# end of dBZi and t loops
		
			# output to csv
			logging.info('twcStepsVerif stats written to {}'.format(statsFile))
			with open(statsFile, 'w') as fid:
				fid.write(csvTxt)

#			# age oldest twcStepsVerif output files
#			syscall('find {} -type f -maxdepth 1 -mmin +{} -exec rm {{}} \;'.format(outputDir, cfg['twcStepsVerif']['ageingDelay']))



#----------------------------------------------------------------------------------------#

# main watch loop
if __name__ == "__main__":

	# load config file
	with open(sys.argv[1], 'r') as ymlfile:
		cfg = yaml.load(ymlfile)

	# setup logging
	logging.basicConfig(filename=cfg['twcStepsVerif']['logFile'], level=logging.getLevelName(cfg['twcStepsVerif']['logLevel']), format='%(asctime)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
	logging.debug('Config file {} read on startup'.format(sys.argv[1]))
	logging.debug('Polling set at {} seconds'.format(cfg['twcStepsVerif']['pollingInterval']))

	# setup observer. must be polling to work on NFS
	observer = PollingObserver(timeout=cfg['twcStepsVerif']['pollingInterval'])
	observer.start()

	# dictionnary of watchs, empty to begin with
	watch = dict();

	# choose watch directory as a function of verification mode (operational or hindcast)
	if cfg['twcStepsVerif']['operationalMode']:
		watchDir = cfg['twcStepsVerif']['reanalysisPath']
		logging.info('Operational mode: watching reanalysis files in {}'.format(watchDir))
	else:
		watchDir = cfg['twcStepsVerif']['fcPath']
		logging.info('Hindcast mode: watching forecast files in {}'.format(watchDir))

	# sit and watch
	try:
		while True:

			# add or remove any watches if changes to the ID list are detected
			if set(watch.keys()) != set(cfg['twcStepsVerif']['ids']):

				# ids to add, but only if the watch directory exists already
				for id_ in list(set(cfg['twcStepsVerif']['ids']) - set(watch.keys())):
					watchPath = '{}/{}'.format(watchDir, id_)
					if os.path.exists(watchPath):
						watch[id_] = observer.schedule(MyWatch(id_), path=watchPath, recursive=False)
						logging.info('IDR{} added to twcStepsVerif watch'.format(id_))

				# ids to remove
				for id_ in list(set(watch.keys()) - set(cfg['twcStepsVerif']['ids'])):
					observer.unschedule(watch[id_])					
					watch.pop(id_, None)
					logging.info('IDR{} removed from twcStepsVerif Watch'.format(id_))

			# wait for the next config file update check
			# note that this is a non-blocking sleep, files are still watched
			time.sleep(cfg['twcStepsVerif']['configRefresh'])

			# load config file
			with open(sys.argv[1], 'r') as ymlfile:
				cfg = yaml.load(ymlfile)
				logging.getLogger().setLevel(cfg['twcStepsVerif']['logLevel'])
				logging.debug('Config file {} updated'.format(sys.argv[1]))
				logging.debug('Polling set at {} seconds'.format(cfg['twcStepsVerif']['pollingInterval']))

	except KeyboardInterrupt:
		observer.stop()

	# clean up 
	observer.join()

