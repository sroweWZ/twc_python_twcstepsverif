#!/usr/bin/python3

"""
Utility functions for STEPS verification
^^^^^^^^^^^^

.. autosummary::
   :nosignatures:
   :toctree: generated/


"""

# standard libraries
import sys
import io
import os
import logging

# site packages
import h5py
import numpy as np
from scipy.interpolate import griddata, interp1d, LinearNDInterpolator
import scipy.ndimage as ndimage

# ATTENTION: Needs to be imported AFTER h5py, otherwise ungraceful crash
import netCDF4 as nc
import matplotlib
matplotlib.use('Agg')	# refrain from graphic output
import matplotlib.pyplot as plt


# ------------------------------------------------------------------------------------------------------------------ #

# read in scans (ppis or cappis) from L3 volradar or twcStep netCDF files
def readNetCdfScans(filename):

	ncfile = nc.Dataset(filename, mode='r')

	# read variables
	lats = ncfile.variables['lat'][:]
	lons = ncfile.variables['lon'][:]
	hght = 0 #ncfile.variables['hght'][:,:,:]
	scanTime = ncfile.variables['time'][:]
	dbzh = ncfile.variables['refl'][:,:,:]
	vradh = 0 #ncfile.variables['radv'][:,:,:]

	# close up
	ncfile.close()

	return scanTime, lons, lats, hght, dbzh, vradh


# read in the given variables from netCDF
def readNetCdfVar(filename, varnames):

	ncfile = nc.Dataset(filename, mode='r')

	# read variables
	lats = ncfile.variables['lat'][:]
	lons = ncfile.variables['lon'][:]
	scanTime = ncfile.variables['time'][:]

	data = {}
	for i in range(len(varnames)):
		if ncfile.variables[varnames[i]].ndim == 3:
			data[varnames[i]] = ncfile.variables[varnames[i]][:,:,:]
		elif ncfile.variables[varnames[i]].ndim == 2:
			data[varnames[i]] = ncfile.variables[varnames[i]][:,:]
		elif ncfile.variables[varnames[i]].ndim == 4:
			data[varnames[i]] = ncfile.variables[varnames[i]][:,:,:,:]

	# close up
	ncfile.close()

	return scanTime, lons, lats, data


# read in a scantime (ppis or cappis) from L3 volradar or twcSteps netCDF files
def readNetCdfScanTime(filename, scanIndex):

	ncfile = nc.Dataset(filename, mode='r')

	# read variables
	scanTime = ncfile.variables['time'][:]

	# close up
	ncfile.close()

	return scanTime


# initialise a netCDF file with basic dimensions, fields to be appended later
def initialiseNetCdf(filename, times, dbzthresholds, lons, lats):

	ncfile = nc.Dataset(filename, 'w', format='NETCDF4')

	# dimensions
	lon_dim = ncfile.createDimension('lon', len(lons))			# longitude axis
	lat_dim = ncfile.createDimension('lat', len(lats))			# latitude axis
	dbz_dim = ncfile.createDimension('dBZThreshold', None)		# dbz threshold axis
	time_dim = ncfile.createDimension('time', None)				# time unlimited axis

	# netCDF attributes
	ncfile.author = 'twcSteps'
	ncfile.title = filename

	# define coordinate variables
	lat = ncfile.createVariable('lat', np.float32, ('lat',))
	lat.units = 'degrees_north'
	lat.long_name = 'latitude'

	lon = ncfile.createVariable('lon', np.float32, ('lon',))
	lon.units = 'degrees_east'
	lon.long_name = 'longitude'

	time = ncfile.createVariable('time', np.float32, ('time',))
	time.units = 'seconds since 1970-01-01'
	time.long_name = 'time'

	dbz = ncfile.createVariable('dBZThreshold', np.float32, ('dBZThreshold',))
	dbz.units = 'dBZ'
	dbz.long_name = 'dBZ threshold'

	# write latitudes, longitudes, altitudes, etc
	lat[:] = lats
	lon[:] = lons
	time[:] = times
	dbz[:] = dbzthresholds

	ncfile.close()


# append data to an initialised netCDF file
def appendToNetCdf(filename, field):

	ncfile = nc.Dataset(filename, 'a', format='NETCDF4')

	fld = ncfile.createVariable(field['name'], np.float32, field['dimensions'], zlib=True, complevel=1, shuffle=False)  # note: unlimited dimension is leftmost
	fld.units = field['units']
	fld.standard_name = field['cfname']

	# and the data
	if field['data'].ndim == 3:
		fld[:,:,:] = field['data']
	elif field['data'].ndim == 4:
		fld[:,:,:,:] = field['data']

	# close up
	ncfile.close()


