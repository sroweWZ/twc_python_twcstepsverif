#! /bin/bash

# Paths 
ROOT=/opt/twc_python_twcstepsverif/
PROCESS=twcStepsReanalysis
RESOURCES=${ROOT}resources/twcstepsreanalysis.staging.yml
PID=/var/run/twcstepsverif/${PROCESS}.pid
LOG=/var/log/twcstepsverif/${PROCESS}.log

# Run the python program
${ROOT}${PROCESS}.py ${RESOURCES} >> $LOG &
echo $! > ${PID}
