#!/usr/bin/python3

# --------------------------------------------------------------------------------------------------------------------------------------
#
# TWC Steps Reanalysis
#
# This script:
#	- loads a list of radar IDs
#	- watchs for their ppis or cappis
#	- calculates time until onset/termination of rain based on this and preceeding ppis/cappis
#
# --------------------------------------------------------------------------------------------------------------------------------------

from datetime import datetime
from glob import glob
import h5py
import logging
import multiprocessing
import netCDF4 as nc
import numpy as np
import os
import re
from subprocess import call
import sys
#import threading
import time
from watchdog.observers.polling import PollingObserver
from watchdog.events import FileSystemEventHandler
import yaml

import libTwcStepsVerif
#from libFSS import statsFSS

#----------------------------------------------------------------------------------------#

def syscall(cmd):
	return call(cmd, shell=True, stderr=open(os.devnull, 'w'), stdout=open(os.devnull, 'w'), close_fds=True)

# threaded watch execution
class MyWatch(FileSystemEventHandler):

	def __init__(self, id_):
		self.id = id_
		self.pattern = 'IDR{}_'.format(id_)


	def on_modified(self, event):

		# file may just have been deleted, in which case we care not for its modification
		if os.path.exists(event.src_path):
			self.on_created(event)

	def on_created(self, event):

		# does the created file, not directory, match our watch pattern?
		if not event.is_directory and self.pattern in event.src_path:

			# divi out work to separate processes. note calling active_children() will conveniently clean up any zombie processes
			while len(multiprocessing.active_children()) >= cfg['twcStepsReanalysis']['threadLimit']:
				time.sleep(cfg['twcStepsReanalysis']['threadPollingInterval'])

			logging.debug('Thread {} of maximum {}'.format(len(multiprocessing.active_children()) + 1, cfg['twcStepsReanalysis']['threadLimit']))
			multiprocessing.Process(name='twcStepsReanalysisThread', target=self.runTwcStepsReanalysis, args=(event,)).start()


	def runTwcStepsReanalysis(self, event):

		obsFile = event.src_path

		# does the created file, not directory, match our watch pattern?
		if not event.is_directory and self.pattern in obsFile:

			# vain attempt to wait until all preceeding rapic files have had a chance to be processed
			# they *usually* appear within a second of each other so we shouldn't have to wait long
			# TODO determine the names of files that should appear (not so easy) and wait for all to appear
			time.sleep(1)

			# directories, filenames, lengths
			obsDir = '{}/{}'.format(cfg['twcStepsReanalysis']['obsPath'], self.id)
			reanalysisDir = '{}/{}'.format(cfg['twcStepsReanalysis']['reanalysisPath'], self.id)
			ndbz, nt = len(cfg['twcStepsReanalysis']['outputDBzThresholds']), cfg['twcStepsReanalysis']['nLeadtimes']

			# create stats directory if it doesn't exist
			if not os.path.exists(reanalysisDir):
				os.makedirs(reanalysisDir)

			# grab a list of past radar files, from oldest to most recent ie retaining chronological order
			obsFiles = sorted(glob('{}/*.nc'.format(obsDir)))

			# retain only files from oldest to the most recent
			obsFiles = obsFiles[0:obsFiles.index(obsFile)]

			# we need at least nLeadTimes worth of radar images before this one 
			if len(obsFiles) < nt:
				logging.warning('Only found {} past radar images of the {} needed: is your radar L3 aging too aggressive?'.format(len(obsFiles), nt))
				return

			# crop to cfg['twcStepsVerif']['nLeadtimes'] obsFiles
			obsFiles = obsFiles[-nt:]
			assert(len(obsFiles) == nt)

			# look for the forecast file with an analysis time close to the radar observation
			# there are two way to do this
			#	(1) take the nLeadtimes file in the past, however this assume no missing radar files
			#	(2) calculate the theoretical analysis time (t_current - nLeadtimes*dt) and look for a radar file that is close to the derived date
			# (2) is *probably* a more reliable but (1) much easier to implement so will run with it for now TODO
			analysisObsFile = obsFiles[0]
			analysisTimeStr = re.search('IDR.*_([0-9]{14})\..*\.nc', analysisObsFile).group(1)				# TODO don't assume a match ^^

			# ingest the radar netCDF to get dimensions
			obsTime, obsLons, obsLats, _, obsDbzh, _ = libTwcStepsVerif.readNetCdfScans(obsFile)
			nx, ny = len(obsLons), len(obsLats)

			# dt is expected to be either 6 or 10 minutes	# TODO this is perhaps a tad sloppy, should be stored as metadata in the radar file itself
			dt = 6*60 #obsTime[0] - obsTime[1]
#			if abs(dt - 6*60) < 2.5*60:
#				dt = 6.0*60
#			else:
#				dt = 10.0*60

			# loop over images, from most recent towards the past
			# derive "time until onset/termination of rain" fields for various reflectivity thresholds
			obsTimes = np.zeros(nt)
			obsTimeUntilRainOnset = np.nan * np.ones((nt, ndbz, nx, ny))
			obsTimeUntilRainTermination = np.nan * np.ones_like(obsTimeUntilRainOnset)

			for t in reversed(range(nt)):

				# load netCDF
				obsTimes[t], obsLons, obsLats, _, obsDbzh, _ = libTwcStepsVerif.readNetCdfScans(obsFiles[t])
				obsTimeStr = datetime.utcfromtimestamp(obsTimes[t]).strftime('%Y%m%d%H%M%S')

				if t < nt-1:
					obsTimeUntilRainOnset[t] = obsTimeUntilRainOnset[t+1] + dt
					obsTimeUntilRainTermination[t] = obsTimeUntilRainTermination[t+1] + dt

				obsIsRain = np.zeros((ndbz, nx, ny), dtype=bool)
				for dBZi in	range(ndbz):
					obsIsRain[dBZi,:,:] = obsDbzh[0,:,:,cfg['twcStepsReanalysis']['scanIndex']] > (cfg['twcStepsReanalysis']['outputDBzThresholds'][dBZi] + 1.0)	# added +1.0 because of small round-off errors here and there creating noise

				obsTimeUntilRainOnset[t, obsIsRain] = 0
				obsTimeUntilRainTermination[t, ~obsIsRain] = 0


			# save derived output fields to netCDF
			ncFile = '{}/{}/twcSteps_reanalysis_{}_{}.nc'.format(cfg['twcStepsReanalysis']['reanalysisPath'], self.id, self.id, analysisTimeStr)
			libTwcStepsVerif.initialiseNetCdf(ncFile, obsTimes, cfg['twcStepsReanalysis']['outputDBzThresholds'], obsLons, obsLats)	

			libTwcStepsVerif.appendToNetCdf(ncFile, {'name': 'timeUntilRainOnset', 
													'units': 's', 
													'dimensions': ('time', 'dBZThreshold', 'lat', 'lon'), 
													'cfname': 'time_until_rain_onset', 
													'data': obsTimeUntilRainOnset})

			libTwcStepsVerif.appendToNetCdf(ncFile, {'name': 'timeUntilRainTermination', 
													'units': 's', 
													'dimensions': ('time', 'dBZThreshold', 'lat', 'lon'), 
													'cfname': 'time_until_rain_termination', 
													'data': obsTimeUntilRainTermination})

			logging.info('twcStepsReanalysis written to {}'.format(ncFile))

#			# age oldest twcStepsVerif output files
			syscall('find {} -type f -maxdepth 1 -mmin +{} -exec rm {{}} \;'.format(reanalysisDir, cfg['twcStepsReanalysis']['ageingDelay']))



#----------------------------------------------------------------------------------------#

# main watch loop
if __name__ == "__main__":

	# load config file
	with open(sys.argv[1], 'r') as ymlfile:
		cfg = yaml.load(ymlfile)

	# setup logging
	logging.basicConfig(filename=cfg['twcStepsReanalysis']['logFile'], level=logging.getLevelName(cfg['twcStepsReanalysis']['logLevel']), format='%(asctime)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
	logging.debug('Config file {} read on startup'.format(sys.argv[1]))
	logging.debug('Polling set at {} seconds'.format(cfg['twcStepsReanalysis']['pollingInterval']))

	# setup observer. must be polling to work on NFS
	observer = PollingObserver(timeout=cfg['twcStepsReanalysis']['pollingInterval'])
	observer.start()

	# dictionnary of watchs, empty to begin with
	watch = dict();

	# sit and watch
	try:
		while True:

			# add or remove any watches if changes to the ID list are detected
			if set(watch.keys()) != set(cfg['twcStepsReanalysis']['ids']):

				# ids to add, but only if the watch directory exists already
				for id_ in list(set(cfg['twcStepsReanalysis']['ids']) - set(watch.keys())):
					watchPath = '{}/{}'.format(cfg['twcStepsReanalysis']['obsPath'], id_)
					if os.path.exists(watchPath):
						watch[id_] = observer.schedule(MyWatch(id_), path=watchPath, recursive=False)
						logging.info('IDR{} added to twcStepsReanalysis watch'.format(id_))

				# ids to remove
				for id_ in list(set(watch.keys()) - set(cfg['twcStepsReanalysis']['ids'])):
					observer.unschedule(watch[id_])					
					watch.pop(id_, None)
					logging.info('IDR{} removed from twcStepsReanalysis Watch'.format(id_))

			# wait for the next config file update check
			# note that this is a non-blocking sleep, files are still watched
			time.sleep(cfg['twcStepsReanalysis']['configRefresh'])

			# load config file
			with open(sys.argv[1], 'r') as ymlfile:
				cfg = yaml.load(ymlfile)
				logging.getLogger().setLevel(cfg['twcStepsReanalysis']['logLevel'])
				logging.debug('Config file {} updated'.format(sys.argv[1]))
				logging.debug('Polling set at {} seconds'.format(cfg['twcStepsReanalysis']['pollingInterval']))

	except KeyboardInterrupt:
		observer.stop()

	# clean up 
	observer.join()

