
# weed out 10minute timestep forecasts
grep -l ', 600, 0, ' /opt/twc_python_twcstepsverif/data/stats/71/stats_2017*.csv |xargs rm

# remove headers
tail -qn+3 /opt/twc_python_twcstepsverif/data/stats/71/stats_201711* > /tmp/stats.csv

