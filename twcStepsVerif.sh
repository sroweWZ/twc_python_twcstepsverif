#! /bin/bash

# Paths 
ROOT=/opt/twc_python_twcstepsverif/
PROCESS=twcStepsVerif
RESOURCES=${ROOT}resources/twcstepsverif.staging.yml
PID=/var/run/twcstepsverif/${PROCESS}.pid
LOG=/var/log/twcstepsverif/${PROCESS}.log

# Run the python program
${ROOT}${PROCESS}.py ${RESOURCES} >> $LOG 2>&1 &
echo $! > ${PID}
